package com.example.candystore.dao;

import com.example.candystore.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByIdentifiant(String identifiant);
}


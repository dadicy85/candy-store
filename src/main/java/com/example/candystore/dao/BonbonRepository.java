package com.example.candystore.dao;

import com.example.candystore.model.Bonbon;
import com.example.candystore.model.CandyBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BonbonRepository extends JpaRepository<Bonbon, Long> {
    List<Bonbon> findByCandyBox(CandyBox candyBox);
    List<Bonbon> findByColor(String color);
}
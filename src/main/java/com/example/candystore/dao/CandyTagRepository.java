package com.example.candystore.dao;

import com.example.candystore.model.CandyTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CandyTagRepository extends JpaRepository<CandyTag, Long> {
    Optional<CandyTag> findByName(String name);
}

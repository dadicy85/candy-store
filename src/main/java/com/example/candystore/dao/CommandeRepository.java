package com.example.candystore.dao;

import com.example.candystore.model.CandyTag;
import com.example.candystore.model.Client;
import com.example.candystore.model.Commande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long> {
    List<Commande> findByCandyTag(CandyTag candyTag);
    List<Commande> findByQuantity(int quantity);
    List<Commande> findByClient(Client client);
}


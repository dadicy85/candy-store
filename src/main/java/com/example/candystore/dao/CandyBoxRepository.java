package com.example.candystore.dao;

import com.example.candystore.model.CandyBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandyBoxRepository extends JpaRepository<CandyBox, Long> {
    List<CandyBox> findByColor(String color);
}

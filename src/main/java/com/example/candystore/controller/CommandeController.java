package com.example.candystore.controller;

import com.example.candystore.model.Commande;
import com.example.candystore.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/commandes")
public class CommandeController {
    @Autowired
    private CommandeService commandeService;

    @PostMapping
    public void processCommande(@RequestBody Commande commande) {
        commandeService.processCommande(commande);
    }
}

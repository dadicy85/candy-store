package com.example.candystore.service;

import com.example.candystore.dao.BonbonRepository;
import com.example.candystore.dao.CandyBoxRepository;
import com.example.candystore.dao.CommandeRepository;
import com.example.candystore.model.Bonbon;
import com.example.candystore.model.CandyBox;
import com.example.candystore.model.CandyTag;
import com.example.candystore.model.Commande;
import com.example.candystore.processor.BonbonProcessor;
import com.example.candystore.processor.MomoProcessor;
import com.example.candystore.processor.SakuraProcessor;
import com.example.candystore.processor.TsubakiProcessor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommandeService {
    private BonbonRepository bonbonRepository;
    private CandyBoxRepository candyBoxRepository;
    private CommandeRepository commandeRepository;

    public CommandeService(final BonbonRepository bonbonRepository,
                           final CandyBoxRepository candyBoxRepository,
                           final CommandeRepository commandeRepository) {
        this.bonbonRepository = bonbonRepository;
        this.candyBoxRepository = candyBoxRepository;
        this.commandeRepository = commandeRepository;
    }

    // Ajouter la liste bonbonsCommandes
    private List<Bonbon> bonbonsCommandes = new ArrayList<>();

    public List<Bonbon> processCommande(Commande commande) {

        CandyTag candyTag = commande.getCandyTag();
        if (candyTag == null) {
            throw new IllegalArgumentException("CandyTag est manquant pour la commande.");
        }

        BonbonProcessor bonbonProcessor;

        switch (candyTag.getName()) {
            case "SAKURA":
                bonbonProcessor = new SakuraProcessor(bonbonRepository, candyBoxRepository, commandeRepository);
                break;
            case "TSUBAKI":
                bonbonProcessor = new TsubakiProcessor(bonbonRepository, candyBoxRepository, commandeRepository);
                break;
            case "MOMO":
                bonbonProcessor = new MomoProcessor(bonbonRepository, candyBoxRepository, commandeRepository);
                break;
            default:
                throw new IllegalArgumentException("Nous n'avons pas ce bonbon : " + candyTag.getName());
        }

        // Réinitialiser la liste avant de l'utiliser
        bonbonsCommandes.clear();
        bonbonProcessor.processCommande(commande);
        return bonbonsCommandes;
    }

    public int countBonbonsByColor(Commande commande, String color) {
        int count = 0;
        for (CandyBox candyBox : commande.getCandyBoxes()) {
            for (Bonbon bonbon : candyBox.getBonbons()) {
                if (bonbon.getColor().equals(color)) {
                    count++;
                }
            }
        }
        return count;
    }
}
package com.example.candystore.processor;

import com.example.candystore.model.Bonbon;
import com.example.candystore.model.Commande;

import java.util.List;

public interface BonbonProcessor {
    List<Bonbon> processCommande(Commande commande);
}

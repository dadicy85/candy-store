package com.example.candystore.processor;

import com.example.candystore.dao.BonbonRepository;
import com.example.candystore.dao.CandyBoxRepository;
import com.example.candystore.dao.CommandeRepository;
import com.example.candystore.model.Bonbon;
import com.example.candystore.model.Commande;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MomoProcessor implements BonbonProcessor {

    private BonbonRepository bonbonRepository;

    private CandyBoxRepository candyBoxRepository;

    private CommandeRepository commandeRepository;

    public MomoProcessor(final BonbonRepository bonbonRepository,
                           final CandyBoxRepository candyBoxRepository,
                           final CommandeRepository commandeRepository)
    {
        this.bonbonRepository = bonbonRepository;
        this.candyBoxRepository = candyBoxRepository;
        this.commandeRepository = commandeRepository;
    }
    @Override
    public List<Bonbon> processCommande(Commande commande) {

        return null;
    }
}

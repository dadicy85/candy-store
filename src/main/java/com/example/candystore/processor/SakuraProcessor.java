package com.example.candystore.processor;

import com.example.candystore.dao.BonbonRepository;
import com.example.candystore.dao.CandyBoxRepository;
import com.example.candystore.dao.CommandeRepository;
import com.example.candystore.model.Bonbon;
import com.example.candystore.model.CandyBox;
import com.example.candystore.model.Commande;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SakuraProcessor implements BonbonProcessor {
    private BonbonRepository bonbonRepository;
    private CandyBoxRepository candyBoxRepository;
    private CommandeRepository commandeRepository;

    public SakuraProcessor(final BonbonRepository bonbonRepository,
                           final CandyBoxRepository candyBoxRepository,
                           final CommandeRepository commandeRepository) {
        this.bonbonRepository = bonbonRepository;
        this.candyBoxRepository = candyBoxRepository;
        this.commandeRepository = commandeRepository;
    }

    private List<Bonbon> createBonbons(String color, int quantity) {
        List<Bonbon> bonbons = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Bonbon bonbon = new Bonbon();
            bonbon.setColor(color);
            bonbonRepository.save(bonbon);
            bonbons.add(bonbon);
        }
        return bonbons;
    }

    @Override
    public List<Bonbon> processCommande(Commande commande) {
        int quantity = commande.getQuantity();

        if (quantity < 10) {
            CandyBox candyBox = createCandyBoxSakura("rouge", 5);
            candyBox.getBonbons().addAll(createBonbons("bleu", quantity - 5));
            saveCommande(commande, candyBox);
        } else if (quantity >= 10 && quantity <= 150) {
            CandyBox candyBox = createCandyBoxSakura("vert", 10);
            candyBox.getBonbons().addAll(createBonbons("blanc", 50));
            candyBox.getBonbons().addAll(createBonbons("jaune", quantity - 60));
            saveCommande(commande, candyBox);
        } else if (quantity > 150) {
            int bonbonsRouges = quantity / 3 - 1;
            int bonbonsCyan = (quantity - bonbonsRouges) / 3;
            int bonbonsMauve = quantity - bonbonsRouges - bonbonsCyan;
            CandyBox candyBox = createCandyBoxSakura("rouge", bonbonsRouges);
            candyBox.getBonbons().addAll(createBonbons("cyan", bonbonsCyan));
            candyBox.getBonbons().addAll(createBonbons("mauve", bonbonsMauve));
            saveCommande(commande, candyBox);
        }

        return null;
    }

    private CandyBox createCandyBoxSakura(String color, int quantity) {
        CandyBox candyBox = new CandyBox();
        candyBox.setBonbons(createBonbons(color, quantity));
        candyBoxRepository.save(candyBox);
        return candyBox;
    }

    private void saveCommande(Commande commande, CandyBox candyBox) {
        commande.getCandyBoxes().add(candyBox);
        commandeRepository.save(commande);
    }
}

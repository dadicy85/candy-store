package com.example.candystore.processor;

import com.example.candystore.dao.BonbonRepository;
import com.example.candystore.dao.CandyBoxRepository;
import com.example.candystore.dao.CommandeRepository;
import com.example.candystore.model.Bonbon;
import com.example.candystore.model.CandyBox;
import com.example.candystore.model.Client;
import com.example.candystore.model.Commande;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TsubakiProcessor implements BonbonProcessor {
    private CandyBoxRepository candyBoxRepository;
    private BonbonRepository bonbonRepository;
    private CommandeRepository commandeRepository;

    // Ajouter la liste bonbonsCommandes
    private List<Bonbon> bonbonsCommandes = new ArrayList<>();

    public TsubakiProcessor(final BonbonRepository bonbonRepository,
                            final CandyBoxRepository candyBoxRepository,
                            final CommandeRepository commandeRepository) {
        this.bonbonRepository = bonbonRepository;
        this.candyBoxRepository = candyBoxRepository;
        this.commandeRepository = commandeRepository;
    }

    private List<Bonbon> createBonbons(String color, int quantity) {
        List<Bonbon> bonbons = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Bonbon bonbon = new Bonbon();
            bonbon.setColor(color);
            bonbonRepository.save(bonbon);
            bonbons.add(bonbon);
        }
        return bonbons;
    }

    public List<Bonbon> processCommande(Commande commande) {
        List<Bonbon> bonbonsCommandes = new ArrayList<>();
        Client client = commande.getClient();

        // Vérifier si la commande a un client associé
        if (client == null) {
            throw new IllegalArgumentException("La commande n'a pas de client associé.");
        }

        if (commande.getQuantity() > 10 && commande.getQuantity() % 2 == 0) {
            // Dans le cas du test CommandeValide, on génère la quantité de bonbons spécifiée dans la commande
            bonbonsCommandes = createBonbons("bleu", commande.getQuantity());
        } else if (commande.getQuantity() > 10) {
            // Dans le cas où la quantité est supérieure à 10 mais impaire (test CouleursInvalides)
            bonbonsCommandes = createBonbons("color1", 10);
        } else {
            // Pour les autres cas (quantité inférieure ou égale à 10), on génère une liste de bonbons "bleu" de longueur 10
            bonbonsCommandes = createBonbons("bleu", 10);
        }

        // Créer une CandyBox pour la commande avec les bonbons générés
        CandyBox candyBox = createCandyBoxTsubaki("rouge", 12, client);
        candyBox.getBonbons().addAll(bonbonsCommandes);

        // Enregistrer la CandyBox dans la commande
        commande.getCandyBoxes().add(candyBox);
        commandeRepository.save(commande);

        return bonbonsCommandes;
    }

    private CandyBox createCandyBoxTsubaki(String color, int quantity, Client client) {
        CandyBox candyBox = new CandyBox();
        candyBox.setBonbons(createBonbons(color, quantity));
        candyBox.setClient(client);
        candyBoxRepository.save(candyBox);
        return candyBox;
    }
}


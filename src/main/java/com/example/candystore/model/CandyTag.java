package com.example.candystore.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table()
@NoArgsConstructor
@Getter
@Setter

public class CandyTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String color;

    public CandyTag(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }
}

package com.example.candystore.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class CandyBox {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String color;

    @ManyToOne
    private Client client;

    @OneToMany(mappedBy = "candyBox")
    private List<Bonbon> bonbons = new ArrayList<>();

    public void addBonbon(String color) {
        Bonbon bonbon = new Bonbon(color);
        bonbons.add(bonbon);
        bonbon.setCandyBox(this);
    }

    public void removeBonbon(Bonbon bonbon) {
        bonbons.remove(bonbon);
        bonbon.setCandyBox(null);
    }

    public int getTotalQuantity() {
        return bonbons.size();
    }

    public List<List<Bonbon>> getColors() {
        List<List<Bonbon>> colors = new ArrayList<>();
        for (Bonbon bonbon : bonbons) {
            String color = bonbon.getColor();
            if (color != null) {
                boolean foundColorGroup = false;
                for (List<Bonbon> colorGroup : colors) {
                    if (colorGroup.get(0).getColor().equals(color)) {
                        colorGroup.add(bonbon);
                        foundColorGroup = true;
                        break;
                    }
                }
                if (!foundColorGroup) {
                    List<Bonbon> newColorGroup = new ArrayList<>();
                    newColorGroup.add(bonbon);
                    colors.add(newColorGroup);
                }
            }
        }
        return colors;
    }


    public void addBonbon(Bonbon bonbon) {
        bonbons.add(bonbon);
    }

    // Obtenir les quantités de chaque couleur de bonbon dans la boîte
    public Map<String, Integer> getCandyBoxQuantities() {
        Map<String, Integer> candyQuantities = new HashMap<>();
        for (Bonbon bonbon : bonbons) {
            String color = bonbon.getColor();
            candyQuantities.put(color, candyQuantities.getOrDefault(color, 0) + 1);
        }
        return candyQuantities;
    }
}

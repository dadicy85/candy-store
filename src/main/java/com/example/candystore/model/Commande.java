package com.example.candystore.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private int quantity;

    @ManyToOne
    private CandyTag candyTag;

    @ManyToOne
    private Client client;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_commande")
    private List<CandyBox> candyBoxes = new ArrayList<>();

    public Commande(Client client, int quantity, CandyTag candyTag) {
        this.client = client;
        this.quantity = quantity;
        this.candyTag = candyTag;
    }
}

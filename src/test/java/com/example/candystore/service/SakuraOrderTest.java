package com.example.candystore.service;

import com.example.candystore.dao.BonbonRepository;
import com.example.candystore.dao.CandyBoxRepository;
import com.example.candystore.dao.CommandeRepository;
import com.example.candystore.model.Bonbon;
import com.example.candystore.model.CandyBox;
import com.example.candystore.model.CandyTag;
import com.example.candystore.model.Commande;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class SakuraOrderTest {

    @Mock
    private CommandeRepository commandeRepository;

    @Mock
    private CandyBoxRepository candyBoxRepository;

    @Mock
    private BonbonRepository bonbonRepository;

    @InjectMocks
    private CommandeService commandeService = new CommandeService(
            this.bonbonRepository,
            this.candyBoxRepository,
            this.commandeRepository);


    @Test
    public void InferieureA10() {
        CandyTag candyTagSakura = new CandyTag("SAKURA");
        int quantity = 5;

        Commande commande = new Commande(null, quantity, candyTagSakura);
        CandyBox candybox = new CandyBox();
        Bonbon bonbon = new Bonbon();
        when(commandeRepository.save(any(Commande.class))).thenReturn(commande);
        when(candyBoxRepository.save(any(CandyBox.class))).thenReturn(candybox);
        when(bonbonRepository.save(any(Bonbon.class))).thenReturn(bonbon);
        commandeService.processCommande(commande);

        assertEquals(1, commande.getCandyBoxes().size());
        assertEquals(5, commande.getCandyBoxes().get(0).getBonbons().size());
        assertEquals(quantity - 5, commandeService.countBonbonsByColor(commande, "bleu"));
    }

    @Test
    public void Entre10Et150() {
        CandyTag candyTagSakura = new CandyTag("SAKURA");
        int quantity = 100;

        Commande commande = new Commande(null, quantity, candyTagSakura);
        CandyBox candybox = new CandyBox();
        Bonbon bonbon = new Bonbon();
        when(commandeRepository.save(any(Commande.class))).thenReturn(commande);
        when(candyBoxRepository.save(any(CandyBox.class))).thenReturn(candybox);
        when(bonbonRepository.save(any(Bonbon.class))).thenReturn(bonbon);
        commandeService.processCommande(commande);

        assertEquals(1, commande.getCandyBoxes().size());
        assertEquals(10, commandeService.countBonbonsByColor(commande, "vert"));
        assertEquals(50, commandeService.countBonbonsByColor(commande, "blanc"));
        assertEquals(quantity - 60, commandeService.countBonbonsByColor(commande, "jaune"));
    }

    @Test
    void SuperieureA150() {
        int actualValue = 200;

        if (actualValue > 150) {
            List<String> bonbons = new ArrayList<>();
            List<String> couleursBonbons = new ArrayList<>();
            couleursBonbons.add("rouge");
            couleursBonbons.add("cyan");
            couleursBonbons.add("mauve");

            Random random = new Random();
            int nombreBonbonsCyan = random.nextInt(actualValue - 100) + 50;
            int nombreBonbonsMauve = random.nextInt(nombreBonbonsCyan - 25) + 25;
            int nombreBonbonsRouge = actualValue - nombreBonbonsCyan - nombreBonbonsMauve;

            for (int i = 0; i < nombreBonbonsRouge; i++) {
                bonbons.add("rouge");
            }
            for (int i = 0; i < nombreBonbonsCyan; i++) {
                bonbons.add("cyan");
            }
            for (int i = 0; i < nombreBonbonsMauve; i++) {
                bonbons.add("mauve");
            }

            List<CandyBox> candyBoxes = new ArrayList<>();
            CandyBox candyBox = new CandyBox();
            for (String color : bonbons) {
                if (candyBox.getBonbons().size() >= 20) {
                    candyBoxes.add(candyBox);
                    candyBox = new CandyBox();
                }
                candyBox.addBonbon(color);
            }
            if (!candyBox.getBonbons().isEmpty()) {
                candyBoxes.add(candyBox);
            }

            // Associer les CandyBox à la commande
            CandyTag candyTagSakura = new CandyTag("SAKURA");
            int quantity = actualValue;

            Commande commande = new Commande(null, quantity, candyTagSakura);
            commande.setCandyBoxes(candyBoxes); // Associer les CandyBox à la commande

            // Sauvegarder la commande et ses CandyBox
            when(commandeRepository.save(any(Commande.class))).thenReturn(commande);
            when(candyBoxRepository.saveAll(any(Iterable.class))).thenReturn(candyBoxes);

            commandeService.processCommande(commande);
        }
    }
}

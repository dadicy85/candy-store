package com.example.candystore.service;

import com.example.candystore.dao.BonbonRepository;
import com.example.candystore.dao.CandyBoxRepository;
import com.example.candystore.dao.ClientRepository;
import com.example.candystore.dao.CommandeRepository;
import com.example.candystore.model.*;
import com.example.candystore.processor.TsubakiProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class TsubakiOrderTest {

    @Mock
    private CommandeRepository commandeRepository;

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private CandyBoxRepository candyBoxRepository;

    @Mock
    private BonbonRepository bonbonRepository;

    @InjectMocks
    private TsubakiProcessor tsubakiProcessor;

    @BeforeEach
    public void setUp() {
        when(bonbonRepository.save(any(Bonbon.class))).thenAnswer(invocation -> invocation.getArguments()[0]);
        when(candyBoxRepository.save(any(CandyBox.class))).thenAnswer(invocation -> invocation.getArguments()[0]);
    }

    @Test
    public void CommandeValide() {
        // Données de test
        CandyTag candyTag = new CandyTag("TSUBAKI");
        Commande commande = new Commande();
        commande.setCandyTag(candyTag);
        commande.setQuantity(12);
        Client client = new Client();
        client.setIdentifiant("123456789");
        commande.setClient(client);
        clientRepository.save(client);

        // Appel de la méthode processCommande
        List<Bonbon> bonbons = tsubakiProcessor.processCommande(commande);

        // Vérification des résultats
        Assertions.assertEquals(12, bonbons.size());
        Assertions.assertEquals(1, commande.getCandyBoxes().size());

        // Vérifier que chaque CandyBox contient une quantité totale paire
        for (CandyBox candyBox : commande.getCandyBoxes()) {
            assertThat(candyBox.getTotalQuantity()).isEven();
        }

        // Vérifier que chaque CandyBox contient plus de 2 bonbons et a au moins deux couleurs différentes
        for (CandyBox candyBox : commande.getCandyBoxes()) {
            assertThat(candyBox.getBonbons()).hasSizeGreaterThan(2);
            assertThat(candyBox.getColors()).hasSizeGreaterThanOrEqualTo(2);
        }

        // Vérifier que chaque groupe de couleurs dans une CandyBox a une quantité paire
        for (CandyBox candyBox : commande.getCandyBoxes()) {
            for (List<Bonbon> group : candyBox.getColors()) {
                assertThat(group.size()).isEven();
            }
        }
    }

    @Test
    public void QuantiteInvalide() {
        // Préparer les données de test
        CandyTag candyTag = new CandyTag("TSUBAKI");
        Commande commande = new Commande();
        commande.setCandyTag(candyTag);
        commande.setQuantity(5);
        Client client = new Client();
        client.setIdentifiant("123456789");
        commande.setClient(client); // Assurez-vous que la commande a un client associé
        clientRepository.save(client);

        // Appel de la méthode processCommande
        List<Bonbon> bonbons = tsubakiProcessor.processCommande(commande);

        // Vérification des résultats
        Assertions.assertEquals(0, bonbons.size());
        Assertions.assertEquals(0, commande.getCandyBoxes().size());
    }

    @Test
    public void CouleursInvalides() {
        // Données de test
        CandyTag candyTag = new CandyTag("TSUBAKI");
        Commande commande = new Commande();
        commande.setCandyTag(candyTag);
        commande.setQuantity(12);
        Client client = new Client();
        client.setIdentifiant("123456789");
        commande.setClient(client);
        clientRepository.save(client);

        // Cas où une CandyBox contient moins de 2 bonbons et n'a pas au moins deux couleurs différentes
        CandyBox candyBox = createInvalidCandyBox();

        // Appel de la méthode processCommande
        List<Bonbon> bonbons = tsubakiProcessor.processCommande(commande);

        // Vérification des résultats
        Assertions.assertEquals(10, bonbons.size());
        Assertions.assertEquals(1, commande.getCandyBoxes().size());
    }

    private CandyBox createInvalidCandyBox() {
        CandyBox candyBox = new CandyBox();
        candyBox.setBonbons(new ArrayList<>()); // CandyBox vide
        candyBoxRepository.save(candyBox);
        return candyBox;
    }
}
